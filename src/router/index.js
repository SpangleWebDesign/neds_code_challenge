import Vue from 'vue'
import VueRouter from 'vue-router'
import Index from '../components/Index.vue'
import HorseRaces from '../components/HorseRaces.vue'
import GreyhoundRaces from '../components/GreyhoundRaces.vue'
import HarnessRaces from '../components/HarnessRaces.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Index',
    component: Index
  },
  {
    path: '/horse-races',
    name: 'HorseRaces',
    component: HorseRaces
  },
  {
    path: '/geryhound-races',
    name: 'GreyhoundRaces',
    component: GreyhoundRaces
  },
  {
    path: '/harness-races',
    name: 'HarnessRaces',
    component: HarnessRaces
  },
]

const router = new VueRouter({
  routes
})

export default router
